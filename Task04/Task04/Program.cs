﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please type in your numbers");
            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());

            Console.WriteLine(calculation1(a, b));
            Console.WriteLine(calculation2(a, b));
            Console.WriteLine(calculation3(a, b));
            Console.WriteLine(calculation4(a, b));
        }

        static string calculation1(double a, double b)
        {
            return $"{a} + {b} = {a + b}";
        }

        static string calculation2(double a, double b)
        {
            return $"{a} - {b} = {a - b}";
        }

        static string calculation3(double a, double b)
        {
            return $"{a} / {b} = {a / b}";
        }

        static string calculation4(double a, double b)
        {
            return $"{a} * {b} = {a * b}";
        }
    }
}
